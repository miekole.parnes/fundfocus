/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    fontFamily: { sans: 'Montserrat, sans-serif' },
    extend: {
      colors: {
        main: '#fbdd90',
        secondary: '#332E2F',
        text: '#fce7b1',
        text2: '#383233',
      },
      screens: {
        wide: '1900px',
        'mobile-s': '320px',
        'mobile-m': '375px',
        'mobile-l': '425px',
      },
    },
  },
  plugins: [],
};
