import { Accounts } from '../Features/Types';

const BASE_URL = `http://localhost:8000`;

export async function createAccount(accounts: Accounts) {
  try {
    const res = await fetch(`${BASE_URL}/accounts`, {
      method: 'POST',
      body: JSON.stringify(accounts),
      headers: { 'Content-Type': 'application/json' },
    });

    const data = await res.json();

    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAccount() {
  try {
    const res = await fetch(`${BASE_URL}/accounts`);
    const data = await res.json();

    return data;
  } catch (error) {
    console.log(error);
  }
}
