import RegisterAlternativeSignUp from './RegisterAlternativeSignUp';
import { Form, Link, useActionData } from 'react-router-dom';
import { createAccount, getAccount } from '../../services/dataApi';
import { Accounts, FormErrors } from '../Types';
import RegisterLoginInfo from './RegisterLoginInfo';
import RegisterPersonalInfo from './RegisterPersonalInfo';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getAllAccount } from '../accounts/accountSlice';
import { useData } from '../context/DataContext';

function RegisterPage() {
  const [password, setPassword] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [isFormHidden, setIsFormHidden] = useState<boolean>(true);

  const formData = useActionData() as FormErrors;

  const dispatch = useDispatch();

  const {
    isMenuOpenContextValue: { setIsMenuOpen },
  } = useData();

  function handleOnClick() {
    setIsFormHidden(false);
  }

  useEffect(
    function () {
      async function fetchData() {
        const accounts = await getAccount();
        dispatch(getAllAccount(accounts));
      }

      fetchData();
    },
    [dispatch],
  );

  useEffect(function () {
    window.scrollTo({ top: 0, behavior: 'instant' });
  }, []);

  useEffect(
    function () {
      setIsMenuOpen(false);
    },
    [setIsMenuOpen],
  );

  return (
    <div
      className={`flex min-h-dvh flex-auto flex-col justify-center bg-no-repeat sm:items-center sm:justify-center sm:bg-center lg:flex-row lg:items-start lg:bg-[url('/src/assets/img/background4.png')] lg:bg-auto lg:bg-right-top`}
    >
      <div
        className={`my-20 ${
          isFormHidden ? 'hidden' : 'flex'
        } w-full flex-col items-center rounded-lg bg-white px-3 sm:w-8/12 sm:py-10 md:border md:drop-shadow lg:flex lg:w-5/12 lg:py-0 lg:pt-10 xl:w-4/12 2xl:w-3/12`}
      >
        <div className='flex items-center justify-center sm:w-full lg:mb-8'>
          <p className='whitespace-pre-wrap px-5 text-center text-4xl font-semibold text-text2'>
            Create Account
          </p>
        </div>
        <Form method='POST' className='flex w-full flex-col space-y-8'>
          <RegisterLoginInfo
            password={password}
            setPassword={setPassword}
            email={email}
            setEmail={setEmail}
            formData={formData}
          />
          <RegisterPersonalInfo />
          <div className='flex flex-col gap-y-2'>
            <button className='flex flex-grow items-center justify-center rounded-full border bg-white py-3 font-semibold outline-main drop-shadow transition-all hover:bg-main'>
              Submit
            </button>

            <div className='flex items-center gap-x-1 text-xs'>
              <span className=''>
                By clicking submit you have read and agree to the FundFocus{' '}
                <Link
                  to='#'
                  className='text-blue-400 underline underline-offset-2'
                >
                  Terms & Condition
                </Link>{' '}
                &{' '}
                <Link
                  to='#'
                  className='text-blue-400 underline underline-offset-2'
                >
                  Privacy Policy.
                </Link>
              </span>
            </div>
          </div>
        </Form>
        <div>
          <div className='mb-2 mt-5 flex gap-x-1 text-xs text-text2'>
            <p>Already have an account?</p>
            <button className='cursor-pointer text-blue-400 underline'>
              Sign in now
            </button>
          </div>
        </div>
      </div>
      <div
        className={`${
          isFormHidden ? 'flex' : 'hidden'
        } w-full items-center px-2 sm:w-7/12 lg:hidden`}
      >
        <button
          className='flex-grow rounded-full border bg-white p-2 drop-shadow sm:w-7/12'
          onClick={() => handleOnClick()}
        >
          Create an account
        </button>
      </div>

      <div className='mx-20 mt-60 hidden h-[30rem] border border-y lg:flex'></div>
      {isFormHidden && <RegisterAlternativeSignUp />}
    </div>
  );
}

// eslint-disable-next-line react-refresh/only-export-components
export async function action({ request }: { request: Request }) {
  const formData = await request.formData();
  const data = Object.fromEntries(formData);

  const accountList = await getAccount();

  const account: Accounts = {
    username: data.username,
    password: data.password,
    email: data.email,
    country: data.country,
  };

  const isEmailUnique = !accountList.some(
    (accounts: Accounts) => accounts.email === account.email,
  );

  console.log(account.email);

  const errors: FormErrors = {};
  console.log(errors);

  if (!isEmailUnique && account.email !== '') {
    errors.email = 'Email is already in used';
  }

  if (Object.keys(errors).length > 0) return errors;

  const newAccount = await createAccount(account);
  console.log(newAccount);
  console.log(accountList);

  return null;
}

export default RegisterPage;

<div>
  <div className='mt-5 flex gap-x-1 text-xs text-text'>
    <p>Already have an account?</p>
    <button className='cursor-pointer text-blue-400 underline'>
      Sign in now
    </button>
  </div>
</div>;
