import { useEffect, useState } from 'react';
import { LoginInfo } from '../Types';
import { PiWarningCircleLight } from 'react-icons/pi';

function RegisterLoginInfo({
  password,
  setPassword,
  email,
  setEmail,
  formData,
}: LoginInfo) {
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [isPassMatch, setIsPassMatch] = useState<boolean>(false);

  const [passHasNumber, setPassHasNumber] = useState<boolean | null>(null);
  const [passHasCapitalLetter, setPassHasCapitalLetter] = useState<
    boolean | null
  >(null);
  const [passHasSymbol, setPassHasSymbol] = useState<boolean | null>(null);

  const [errors, setErrors] = useState<boolean[] | null>([]);

  const [isTouched, setIsTouched] = useState(false);
  const [isPasswordInputTouched, setIsPasswordInputTouched] = useState(false);

  function passCheckNum(value: string) {
    const hasNumbers = /[0-9]/.test(value);

    setPassHasNumber(hasNumbers);
  }

  function passCheckCaps(value: string) {
    const hasCapitalLetter = /[A-Z]/.test(value);

    setPassHasCapitalLetter(hasCapitalLetter);
  }

  function passCheckSymbols(value: string) {
    const hasSymbols = /[!";#$%&'()*+,-./:;<=>?@^_`{|}~]/.test(value);

    setPassHasSymbol(hasSymbols);
  }

  useEffect(
    function () {
      const match = password === confirmPassword;
      setIsPassMatch(match);
    },
    [password, confirmPassword, isTouched],
  );

  useEffect(
    function () {
      const errorsArray = [];
      if (passHasNumber === false) {
        errorsArray.push(passHasNumber);
      }
      if (passHasCapitalLetter === false) {
        errorsArray.push(passHasCapitalLetter);
      }
      if (passHasSymbol === false) {
        errorsArray.push(passHasSymbol);
      }

      setErrors(errorsArray);
    },
    [passHasNumber, passHasCapitalLetter, passHasSymbol],
  );

  return (
    <div className='flex flex-col gap-y-3'>
      <div className='group flex w-full flex-col'>
        <label htmlFor='email' className='group mb-1 text-xs'>
          Email <span className='text-red-500'>*</span>
        </label>
        <input
          id='email'
          name='email'
          type='text'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className='group flex-grow rounded-sm border px-1.5 py-2 outline-main'
          placeholder='Email'
          required
        />
        {formData?.email && email !== '' && (
          <div className='mt-2 flex items-center gap-x-1 border border-orange-500 bg-yellow-100 py-2 pl-2 text-sm font-medium text-orange-700'>
            <span>
              <PiWarningCircleLight size={20} />
            </span>
            <p>{formData.email}</p>
          </div>
        )}
      </div>
      <div className='group flex w-full flex-col'>
        <label htmlFor='password' className='group mb-1 text-xs'>
          Password <span className='text-red-500'>*</span>
        </label>
        <input
          id='password'
          name='password'
          type='password'
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
            passCheckNum(e.target.value);
            passCheckCaps(e.target.value);
            passCheckSymbols(e.target.value);
          }}
          onFocus={() => setIsPasswordInputTouched(true)}
          className={`group flex-grow rounded-sm border px-1.5 py-2 outline-main ${
            errors?.length && password !== '' ? 'bg-red-100' : 'bg-white'
          }`}
          placeholder='Password'
        />
        {isPasswordInputTouched &&
          password !== '' &&
          (!passHasNumber || !passHasCapitalLetter || !passHasSymbol) && (
            <div className='mt-2 flex w-full flex-col rounded border border-red-300 bg-red-100 px-2 py-1.5 text-sm text-red-700'>
              {!passHasNumber && (
                <span>Password requires to have atleast one (1) number.</span>
              )}
              {!passHasCapitalLetter && (
                <span>
                  Password requires to have atleast one (1) capital letter.
                </span>
              )}
              {!passHasSymbol && (
                <span>Password requires to have atleast one (1) symbols.</span>
              )}
            </div>
          )}
      </div>
      <div className='group flex w-full flex-col'>
        <label htmlFor='confirmPassword' className='group mb-1 text-xs'>
          Confirm Password <span className='text-red-500'>*</span>
        </label>
        <input
          id='confirmPassword'
          type='password'
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
          onFocus={() => setIsTouched(true)}
          className='group flex-grow rounded-sm border px-1.5 py-2 outline-main'
          placeholder='Confirm Password'
        />
        {!isPassMatch && isTouched && confirmPassword !== '' && (
          <span>Password doesn't match</span>
        )}
      </div>
    </div>
  );
}

export default RegisterLoginInfo;

// {isPasswordInputTouched &&
//   password !== '' &&
//   (!passHasNumber || !passHasCapitalLetter || !passHasSymbol) && (
//     <div className='flex w-full flex-col rounded border border-red-300 bg-red-100 px-2 py-1.5 text-sm text-red-700'>
//       {!passHasNumber && (
//         <span>Password requires to have atleast one (1) number.</span>
//       )}
//       {!passHasCapitalLetter && (
//         <span>
//           Password requires to have atleast one (1) capital letter.
//         </span>
//       )}
//       {!passHasSymbol && (
//         <span>Password requires to have atleast one (1) symbols.</span>
//       )}
//     </div>
//   )}
