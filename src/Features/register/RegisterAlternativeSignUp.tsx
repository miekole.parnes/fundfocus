import { FaXTwitter } from 'react-icons/fa6';
import { FcGoogle } from 'react-icons/fc';
import { IoLogoFacebook } from 'react-icons/io';

function RegisterAlternativeSignUp() {
  return (
    <div className='flex w-full flex-col px-2 sm:w-7/12 lg:mt-80 lg:w-4/12 xl:w-4/12 2xl:w-3/12'>
      <div className='my-5 flex items-center'>
        <div className='flex-grow border-t-2 border-main'></div>
        <span className='mx-5'>Or</span>
        <div className='flex-grow border-t-2 border-main'></div>
      </div>
      <div className='flex flex-grow flex-col gap-y-2 text-secondary'>
        <button className='flex items-center justify-center gap-x-2 rounded border bg-white py-1 text-sm font-medium ring-main transition-all hover:ring sm:text-sm 2xl:text-base'>
          <IoLogoFacebook size={30} color='#0866ff' /> Sign up with Facebook
        </button>
        <button className='flex items-center justify-center gap-x-2 rounded border bg-white py-1 text-sm font-medium ring-main transition-all hover:ring sm:text-sm 2xl:text-base'>
          <FcGoogle size={30} /> Sign up with Gmail
        </button>
        <button className='flex items-center justify-center gap-x-2 rounded border bg-white py-1 text-sm font-medium ring-main transition-all hover:ring sm:text-sm 2xl:text-base'>
          <FaXTwitter size={30} /> Sign up with Twitter
        </button>
      </div>
    </div>
  );
}

export default RegisterAlternativeSignUp;
