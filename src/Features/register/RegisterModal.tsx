import { IoLogoFacebook } from 'react-icons/io';
import { FcGoogle } from 'react-icons/fc';
import { Content } from '../Types';
import { FaXTwitter } from 'react-icons/fa6';

function RegisterModal({ setContent }: Content) {
  return (
    <div className='flex flex-col items-center bg-white px-2 sm:rounded-lg md:border md:py-10 lg:drop-shadow-md xl:pb-5 xl:pt-10 2xl:h-auto 2xl:w-auto'>
      <div className='lg:w-12/12 mb-10 flex w-full justify-center md:w-9/12 2xl:mb-20 2xl:w-8/12'>
        <p className='whitespace-pre-wrap px-5 text-center text-3xl font-semibold'>
          Create Account
        </p>
      </div>
      <div className='flex w-full flex-col items-center gap-y-5 pb-10 sm:px-20'>
        <div className='space-y-2'>
          <p className='text-center text-xl font-medium'>Login details</p>
          <div className='flex w-full 2xl:w-96'>
            <input
              type='text'
              className='flex-grow rounded-sm border px-2 py-1.5 '
              placeholder='Username'
            />
          </div>
          <div className='flex w-full 2xl:w-96'>
            <input
              type='password'
              className='flex-grow rounded-sm border px-2 py-1.5'
              placeholder='Password'
            />
          </div>
          <div className='flex w-full 2xl:w-96'>
            <input
              type='password'
              className='flex-grow rounded-sm border px-2 py-1.5'
              placeholder='Confirm Password'
            />
          </div>
        </div>
        <div className='space-y-2'>
          <p className='text-center text-xl font-medium'>Personal Info</p>
          <div className='flex w-full gap-x-2 2xl:w-96'>
            <input
              type='text'
              className='w-1/2 rounded-sm border px-2 py-1.5'
              placeholder='First name'
            />
            <input
              type='text'
              className='w-1/2 rounded-sm border px-2 py-1.5'
              placeholder='Last name'
            />
          </div>
          <div className='flex w-full gap-x-2 2xl:w-96'>
            <input
              type='email'
              className='flex-grow rounded-sm border px-2 py-1.5'
              placeholder='Email'
            />
          </div>
        </div>

        <button className='mt-5 flex flex-grow justify-center self-end rounded-2xl border bg-white px-4 py-1.5 font-medium drop-shadow-sm 2xl:w-96 2xl:self-auto'>
          Submit
        </button>
      </div>
      <div className='flex w-full items-center'>
        <div className='flex-grow border-t'></div>
        <span className='mx-4'>Or</span>
        <div className='flex-grow border-t'></div>
      </div>
      <div className='flex flex-col gap-y-2 pt-10'>
        <button className='flex items-center justify-center gap-x-2 border p-1 text-sm font-medium sm:px-5 2xl:w-96 2xl:text-base'>
          <IoLogoFacebook size={30} color='#0866ff' /> Sign up with Facebook
        </button>
        <button className='flex items-center justify-center gap-x-2 border p-1 text-sm font-medium sm:px-5 2xl:w-96 2xl:text-base'>
          <FcGoogle size={30} /> Sign up with Gmail
        </button>
        <button className='flex items-center justify-center gap-x-2 border p-1 text-sm font-medium sm:px-5 2xl:w-96 2xl:text-base'>
          <FaXTwitter size={30} /> Sign up with Twitter
        </button>
      </div>
      <div>
        <div className='mt-5 flex gap-x-1 text-xs'>
          <p>Already have an account?</p>
          <button
            className='cursor-pointer text-blue-400 underline'
            onClick={() => setContent('login')}
          >
            Sign in now
          </button>
        </div>
      </div>
    </div>
  );
}

export default RegisterModal;
