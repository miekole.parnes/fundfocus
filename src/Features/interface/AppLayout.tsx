import { Outlet, useLocation } from 'react-router-dom';
import Header from '../header/Header';
import Footer from './Footer';
import { useData } from '../context/DataContext';
import { useEffect } from 'react';

function AppLayout() {
  const pathname = useLocation().pathname;

  const {
    isMenuOpenContextValue: { isMenuOpen, setIsMenuOpen },
  } = useData();

  useEffect(() => {
    if (isMenuOpen) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'visible';
    }
  }, [isMenuOpen, setIsMenuOpen]);

  return (
    <div className={`flex flex-auto flex-col`}>
      {pathname !== '/home' && <Header />}
      <Outlet />
      <Footer />
    </div>
  );
}

export default AppLayout;
