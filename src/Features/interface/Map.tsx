import { useState } from 'react';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';

function Map() {
  const [mapPosition, useMapPosition] = useState<[number, number]>([
    51.505, -0.09,
  ]);

  return (
    <div className='flex h-screen'>
      <MapContainer
        center={mapPosition}
        zoom={13}
        scrollWheelZoom={true}
        className='w-full'
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'
        />
        <Marker position={mapPosition}>
          <Popup>
            A pretty CSS3 popup. <br /> Easily customizable.
          </Popup>
        </Marker>
      </MapContainer>
    </div>
  );
}

export default Map;
