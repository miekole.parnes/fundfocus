import { ReactNode } from 'react';

function Button({ children }: { children: ReactNode }) {
  const mainStyle =
    'mt-5 flex flex-grow justify-center self-end rounded-2xl border bg-white px-4 py-1.5 font-medium drop-shadow-sm 2xl:w-96 2xl:self-auto';

  return (
    <button className='mt-5 flex flex-grow justify-center self-end rounded-2xl border bg-white px-4 py-1.5 font-medium drop-shadow-sm 2xl:w-96 2xl:self-auto'>
      {children}
    </button>
  );
}

export default Button;
