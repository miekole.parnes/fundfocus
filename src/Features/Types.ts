import { Dispatch, SetStateAction } from 'react';

export interface Content {
  content: string;
  setContent: Dispatch<SetStateAction<string>>;
}

export interface Accounts {
  username: string | FormDataEntryValue;
  password: string | FormDataEntryValue;
  email: string | FormDataEntryValue;
  country: string | FormDataEntryValue;
}

export interface LoginInfo {
  password: string;
  setPassword: Dispatch<SetStateAction<string>>;
  email: string;
  setEmail: Dispatch<SetStateAction<string>>;
  formData: FormErrors;
}

export interface FormErrors {
  username?: string;
  email?: string;
}
