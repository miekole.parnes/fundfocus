import { Line } from 'react-chartjs-2';

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { useEffect, useMemo, useState } from 'react';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
);

function Chart() {
  const [selectedDate, setSelectedDate] = useState<string[]>();

  const test = [
    { date: 'January', income: 3000 },
    { date: 'February', income: 2000 },
    { date: 'March', income: 4000 },
    { date: 'April', income: 4000 },
    { date: 'May', income: 8000 },
    { date: 'June', income: 2000 },
    { date: 'July', income: 1500 },
    { date: 'August', income: 2500 },
    { date: 'September', income: 8000 },
    { date: 'October', income: 10000 },
    { date: 'November', income: 6000 },
    { date: 'December', income: 9000 },
  ];

  const test2 = [
    { date: 'January', income: 900 },
    { date: 'February', income: 2500 },
    { date: 'March', income: 4060 },
    { date: 'April', income: 4000 },
    { date: 'May', income: 3050 },
    { date: 'June', income: 5200 },
    { date: 'July', income: 7000 },
    { date: 'August', income: 4500 },
    { date: 'September', income: 2000 },
    { date: 'October', income: 8000 },
    { date: 'November', income: 9000 },
    { date: 'December', income: 8500 },
  ];

  const months = useMemo(() => {
    return [
      'January',
      'Febuary',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
  }, []);

  const weekdays = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
      title: {
        display: true,
        text: 'Chart.js Line Chart',
      },
    },
  };

  const data = {
    labels: selectedDate,
    datasets: [
      {
        label: 'Dataset 1',
        data: test.map((e) => e.income),
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Dataset 2',
        data: test2.map((e) => e.income),
        borderColor: '#000',
        backgroundColor: '#000',
      },
    ],
  };

  useEffect(
    function () {
      setSelectedDate(months);
    },
    [months],
  );

  return (
    <div className='flex h-max w-auto flex-col items-center justify-center bg-white p-5'>
      <button
        onClick={() => {
          selectedDate === months
            ? setSelectedDate(weekdays)
            : setSelectedDate(months);
        }}
      >
        Switch
      </button>
      <Line options={options} data={data} className='flex-grow' />
    </div>
  );
}

export default Chart;
