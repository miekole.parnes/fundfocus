import {
  Dispatch,
  ReactNode,
  SetStateAction,
  createContext,
  useContext,
  useMemo,
  useState,
} from 'react';

type DataContextType = {
  isMenuOpenContextValue: {
    isMenuOpen: boolean;
    setIsMenuOpen: Dispatch<SetStateAction<boolean>>;
  };
  isPricingClickedContextValue: {
    isPricingClicked: boolean;
    setIsPricingClicked: Dispatch<SetStateAction<boolean>>;
  };
};

const DataContext = createContext<DataContextType | undefined>(undefined);

export function DataProvider({ children }: { children: ReactNode }) {
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const [isPricingClicked, setIsPricingClicked] = useState(false);

  const isMenuOpenContextValue = useMemo(() => {
    return { isMenuOpen, setIsMenuOpen };
  }, [isMenuOpen]);

  const isPricingClickedContextValue = useMemo(() => {
    return { isPricingClicked, setIsPricingClicked };
  }, [isPricingClicked]);

  return (
    <DataContext.Provider
      value={{ isMenuOpenContextValue, isPricingClickedContextValue }}
    >
      {children}
    </DataContext.Provider>
  );
}

// eslint-disable-next-line react-refresh/only-export-components
export function useData() {
  const context = useContext(DataContext);
  if (context === undefined) {
    throw new Error('useData must be used within a DataProvider');
  }
  return context;
}
