import { IoLogoFacebook } from 'react-icons/io';
import { FcGoogle } from 'react-icons/fc';
import { FaXTwitter } from 'react-icons/fa6';
import { Link } from 'react-router-dom';
import { Content } from '../Types';
import { useState } from 'react';

function LoginModal({ setContent }: Content) {
  const [check, setCheck] = useState(false);

  return (
    <div className='flex flex-col items-center border bg-white px-2 sm:rounded-lg md:border md:py-10 lg:drop-shadow-xl xl:pb-5 xl:pt-10 2xl:h-auto 2xl:w-auto'>
      <div className='lg:w-12/12 mb-10 flex w-full justify-center md:w-9/12 2xl:mb-20 2xl:w-8/12'>
        <p className='whitespace-pre-wrap px-5 text-center text-3xl font-semibold'>
          Login to manage your funds!
        </p>
      </div>
      <div className='flex w-full flex-col items-center gap-y-1 pb-10 sm:px-20'>
        <div className='flex w-full 2xl:w-96'>
          <input
            type='text'
            className='flex-grow rounded-sm border px-2 py-1.5 '
            placeholder='Username'
          />
        </div>
        <div className='flex w-full 2xl:w-96'>
          <input
            type='password'
            className='flex-grow rounded-sm border px-2 py-1.5'
            placeholder='Password'
          />
        </div>
        <div className='mt-2 flex w-full justify-between 2xl:w-96'>
          <div className='flex items-center gap-x-1'>
            <input
              type='checkbox'
              id='rememberUser'
              className='h-4'
              onChange={() => setCheck(!check)}
              checked={check}
            />
            <label htmlFor='rememberUser' className='text-xs'>
              Remember me?
            </label>
          </div>
          <Link to='#' className='text-xs hover:underline'>
            Forgot password?
          </Link>
        </div>
        <button className='mt-5 flex flex-grow justify-center self-end rounded-2xl border bg-white px-4 py-1.5 font-medium drop-shadow-sm 2xl:w-96 2xl:self-auto'>
          Submit
        </button>
      </div>
      <div className='flex w-full items-center'>
        <div className='flex-grow border-t'></div>
        <span className='mx-4'>Or</span>
        <div className='flex-grow border-t'></div>
      </div>
      <div className='flex flex-col gap-y-2 pt-10'>
        <button className='flex items-center justify-center gap-x-2 border p-1 text-sm font-medium sm:px-5 2xl:w-96 2xl:text-base'>
          <IoLogoFacebook size={30} color='#0866ff' /> Login with Facebook
        </button>
        <button className='flex items-center justify-center gap-x-2 border p-1 text-sm font-medium sm:px-5 2xl:w-96 2xl:text-base'>
          <FcGoogle size={30} /> Login with Gmail
        </button>
        <button className='flex items-center justify-center gap-x-2 border p-1 text-sm font-medium sm:px-5 2xl:w-96 2xl:text-base'>
          <FaXTwitter size={30} /> Login with Twitter
        </button>
      </div>
      <div>
        <div className='mt-5 flex gap-x-1 text-xs'>
          <p>No account yet?</p>
          <button
            className='cursor-pointer text-blue-400 underline'
            onClick={() => setContent('register')}
          >
            Sign up now
          </button>
        </div>
      </div>
    </div>
  );
}

export default LoginModal;
