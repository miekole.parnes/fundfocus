import { createSlice } from '@reduxjs/toolkit';
import { Accounts } from '../Types';

interface InitialAccount {
  accounts: Accounts[];
}

const initialState: InitialAccount = { accounts: [] };

const accSlice = createSlice({
  name: 'account',
  initialState,
  reducers: {
    getAllAccount(state, action) {
      state.accounts = action.payload;
    },
  },
});

export const { getAllAccount } = accSlice.actions;
export const getAcc = (state: { account: InitialAccount }) =>
  state.account.accounts;
export default accSlice.reducer;
