import { useState, Dispatch, SetStateAction } from 'react';

import LoginModal from '../login/LoginModal';
import RegisterModal from '../register/RegisterModal';

function Modal({ onClose }: { onClose: Dispatch<SetStateAction<boolean>> }) {
  const [content, setContent] = useState<string>('login');

  return (
    <div className='fixed inset-0 flex flex-col items-center justify-center'>
      <div
        className='absolute hidden h-full w-full backdrop-blur-sm md:block'
        onClick={() => onClose(false)}
      ></div>
      {content === 'login' ? (
        <LoginModal setContent={setContent} content={content} />
      ) : (
        <RegisterModal setContent={setContent} content={content} />
      )}
    </div>
  );
}

export default Modal;
