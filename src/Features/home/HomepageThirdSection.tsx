import { Dispatch, MutableRefObject, useEffect, useRef } from 'react';
import { FaCheck } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';

function HomepageThirdSection({
  setPricingRef,
}: {
  setPricingRef: Dispatch<MutableRefObject<HTMLDivElement | null>>;
}) {
  const ref = useRef<HTMLDivElement | null>(null);
  const navigate = useNavigate();

  function handleClick() {
    navigate('/register');
  }

  useEffect(
    function () {
      setPricingRef(ref);
    },
    [setPricingRef],
  );

  return (
    <div
      className='mt-10 flex flex-col items-center border-t bg-secondary py-20'
      ref={ref}
    >
      <div className='mt-10 flex w-fit flex-col items-center justify-center px-2 xl:px-60'>
        <p className='self-start font-bold uppercase tracking-wider text-white md:text-xl'>
          Pricing
        </p>
        <p className='my-10 text-center text-3xl font-semibold text-main md:text-start md:text-5xl'>
          Choose the best plan for you
        </p>
      </div>

      <div className='mx-2 flex flex-col justify-center gap-y-10 lg:flex-row lg:gap-x-5 2xl:gap-x-10'>
        <div className='card flex w-full flex-col rounded-3xl border bg-white px-7 pb-10 sm:px-10 md:mx-40 md:w-auto lg:mx-0'>
          <p className='mx-5 mt-5 self-center text-3xl font-bold'>Free Plan</p>
          <div className='my-10 flex flex-col items-center justify-center'>
            <div className='flex items-end'>
              <p className='text-4xl font-semibold'>FREE</p>
            </div>
            <div className='flex items-end'>
              <p className='text-sm text-gray-400'>FREE</p>
            </div>
            <div className='mt-10 self-center'>
              <button
                className='rounded-full border border-main bg-white px-10 py-5 font-semibold transition-all hover:bg-main'
                onClick={() => handleClick()}
              >
                Get Started
              </button>
            </div>
          </div>
          <div className='flex flex-col space-y-5 md:w-60'>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <p className='whitespace-pre-wrap'>Expense Tracking</p>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' /> <span>Basic Budgeting</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span className='flex flex-wrap whitespace-pre-wrap'>
                Transaction History
              </span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span className='self-center'>Bill Reminders</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span>Currency Conversion</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span>Security Measure</span>
            </div>
          </div>
        </div>

        <div
          className={`card relative flex w-full flex-col overflow-hidden rounded-3xl bg-white pb-10 after:absolute after:-right-[15%] after:top-[5%] after:rotate-45 after:bg-main after:px-10 after:py-1 after:font-bold after:uppercase after:content-['Best_Seller'] md:mx-40 md:w-80 lg:mx-0 `}
        >
          {/* <div
            className={`absolute left-32 top-6 flex h-10 w-full rotate-45 items-center justify-center bg-main font-bold tracking-wide `}
          ></div> */}
          <p className='mx-5  mt-5 self-center text-3xl font-bold'>Starter</p>
          <div className='my-10 flex flex-col items-center justify-center'>
            <div className='flex items-end'>
              <p className='text-4xl font-semibold'>$4.99</p>
              <span className='font-medium'>/mo</span>
            </div>
            <div className='flex items-end'>
              <p className='text-sm text-gray-400'>
                or $47.90 billed annually (20% Off)
              </p>
            </div>
            <div className='mt-10 self-center'>
              <button className='rounded-full bg-main px-10 py-5 font-semibold transition-all hover:brightness-105 '>
                Buy Starter Plan
              </button>
            </div>
          </div>
          <div className='flex flex-col space-y-5 px-10 md:w-full'>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <p className='whitespace-pre-wrap'>All Free Plan Features</p>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' /> <span>Bank Account Integration</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span className='flex flex-wrap whitespace-pre-wrap'>
                Advanced Budgeting Tools
              </span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span className='self-center'>Financial Goal Setting</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span>Enhanced Reports</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span>Priority Customer Support</span>
            </div>
          </div>
        </div>

        <div className='card flex w-full flex-col rounded-3xl border bg-white px-7 pb-10 sm:px-10 md:mx-40 md:w-auto lg:mx-0'>
          <p className='mx-5 mt-5 self-center text-3xl font-bold'>Premium</p>
          <div className='my-10 flex flex-col items-center justify-center'>
            <div className='flex items-end'>
              <p className='text-4xl font-semibold'>$9.99</p>
              <span className='font-medium'>/mo</span>
            </div>
            <div className='flex items-end'>
              <p className='text-sm text-gray-400'>
                or $80.31 billed annually (33% Off)
              </p>
            </div>
            <div className='mt-10 self-center'>
              <button className='rounded-full border border-main bg-white px-10 py-5 font-semibold transition-all hover:bg-main'>
                Buy Premium Plan
              </button>
            </div>
          </div>

          <div className='flex flex-col space-y-5 md:w-60'>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <p className='whitespace-pre-wrap'>All Starter Plan Features</p>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' /> <span>Investment Tracking</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span className='flex flex-wrap whitespace-pre-wrap'>
                Expense Receipt Scanning
              </span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span className='self-center'>Credit Score Monitoring</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span>Bill Payment Integration</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span>Multi-device Sync</span>
            </div>
            <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span>Exclusive Financial Advice</span>
            </div>
            {/* <div className='relative flex flex-grow items-center gap-x-5'>
              <FaCheck color='#d7af70' />
              <span>Priority Customer Support</span>
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomepageThirdSection;
