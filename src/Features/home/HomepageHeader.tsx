import { Dispatch, SetStateAction } from 'react';
import { IoMenu } from 'react-icons/io5';

function HomepageHeader({
  handleClickScrollToPricing,
  setIsMenuOpen,
}: {
  handleClickScrollToPricing: () => void;
  setIsMenuOpen: Dispatch<SetStateAction<boolean>>;
}) {
  return (
    <div className='relative flex flex-grow items-center justify-center py-10 text-3xl font-bold md:justify-between lg:text-4xl'>
      <button
        className='absolute left-2 md:hidden'
        onClick={() => setIsMenuOpen(true)}
      >
        <IoMenu size={30} className='text-main' />
      </button>
      <div className='flex items-center font-bold text-white mobile-m:text-4xl md:text-3xl lg:text-5xl'>
        <p className='text-main'>F</p>und <p className='text-main'>F</p>ocus
      </div>
      <div className='hidden gap-x-10 text-lg font-semibold text-white md:flex lg:gap-x-20 lg:text-xl'>
        <p className='cursor-default transition-all hover:text-main'>Home</p>
        <button
          className='transition-all hover:text-main'
          onClick={handleClickScrollToPricing}
        >
          Pricing
        </button>
        <p className='cursor-default transition-all hover:text-main'>
          About us
        </p>
      </div>
    </div>
  );
}

export default HomepageHeader;
