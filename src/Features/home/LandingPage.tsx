import { MdKeyboardDoubleArrowDown } from 'react-icons/md';
// import styles from './LandingPage.module.css';
import { Dispatch, MutableRefObject, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import HomepageHeader from './HomepageHeader';
import Menu from '../header/Menu';
import { useData } from '../context/DataContext';

function LandingPage({
  handleClickScrollToLearnMore,
  handleClickScrollToPricing,
  setTestRef,
}: {
  handleClickScrollToLearnMore: () => void;
  handleClickScrollToPricing: () => void;

  setTestRef: Dispatch<MutableRefObject<HTMLDivElement | null>>;
}) {
  const ref = useRef<HTMLDivElement | null>(null);

  const {
    isMenuOpenContextValue: { setIsMenuOpen },
  } = useData();

  useEffect(() => {
    setTestRef(ref);
  });

  return (
    <div
      className={`flex flex-auto flex-grow flex-col bg-secondary bg-contain bg-right bg-no-repeat px-2 md:px-3`}
      ref={ref}
    >
      <Menu />
      <HomepageHeader
        handleClickScrollToPricing={handleClickScrollToPricing}
        setIsMenuOpen={setIsMenuOpen}
      />
      <div className='mt-10 flex flex-grow flex-col items-center lg:mt-0 lg:flex-row lg:py-5 lg:pb-20 2xl:mx-40'>
        <div className={`flex w-full flex-grow flex-col gap-y-10 wide:px-5`}>
          <div className='flex flex-col gap-y-5 text-center lg:text-start'>
            <div className={`mb-2 flex flex-col gap-y-5 text-main`}>
              <p className={`text-3xl font-bold md:text-4xl xl:text-5xl`}>
                Smart Finance for a Brighter Future.
              </p>
              <p className={`text-2xl font-bold md:text-3xl xl:text-4xl`}>
                FundFocus helps you manage your money wisely.
              </p>
            </div>
            <p
              className={`text-balance rounded py-1.5 text-sm font-medium text-white md:block md:text-lg lg:font-bold lg:tracking-wide`}
            >
              A service that helps you easily keep track of your wealth and
              expenses, anytime, anywhere.
            </p>
          </div>

          <div className='mt-10 flex flex-col items-center justify-center gap-x-5 gap-y-5 lg:mt-20 lg:flex-row lg:gap-x-10 lg:self-start'>
            <div className='flex items-center'>
              <Link
                to='/register'
                className='flex items-center rounded bg-main p-3 text-center font-bold text-text2 drop-shadow sm:text-xl lg:p-5 lg:text-2xl'
              >
                Get Started
              </Link>
            </div>

            <div className='flex flex-grow items-center'>
              <button
                type='button'
                className='group flex items-center gap-x-2 rounded bg-white p-3 font-bold text-text2 drop-shadow sm:text-xl lg:p-5 lg:text-2xl'
                onClick={handleClickScrollToLearnMore}
              >
                Learn more
                <MdKeyboardDoubleArrowDown
                  size={20}
                  className='group group-hover:animate-bounce'
                />
              </button>
            </div>
          </div>
        </div>

        <img
          src='/src/assets/img/img4.png'
          alt=''
          className='my-10 aspect-square rounded-full object-cover sm:h-[28rem] xl:h-[40rem]'
        />
      </div>
    </div>
  );
}

export default LandingPage;
