import { MutableRefObject, useEffect, useState } from 'react';

import LandingPage from './LandingPage';
import HomepageSecondSection from './HomepageSecondSection';
import HomepageThirdSection from './HomepageThirdSection';
import StickyHeaders from '../header/StickyHeaders';
import { useData } from '../context/DataContext';
import { useSearchParams } from 'react-router-dom';

function Homepage() {
  // const [isModalOpen, setIsModalOpen] = useState(false);
  const [ref, setRef] = useState<MutableRefObject<HTMLDivElement | null>>();
  const [contentRef, setContentRef] =
    useState<MutableRefObject<HTMLDivElement | null>>();
  const [isClicked, setIsClicked] = useState(false);

  const [testRef, setTestRef] =
    useState<MutableRefObject<HTMLDivElement | null>>();

  const [pricingRef, setPricingRef] =
    useState<MutableRefObject<HTMLDivElement | null>>();

  const {
    isMenuOpenContextValue: { setIsMenuOpen },
    isPricingClickedContextValue: { isPricingClicked, setIsPricingClicked },
  } = useData();

  const [searchParams, setSearchParams] = useSearchParams();

  function handleClickScrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });

    setSearchParams({});
  }

  function handleClickScrollToPricing() {
    setSearchParams({ click: 'pricing' });
    setIsPricingClicked(true);
  }

  function handleClickScrollToLearnMore() {
    setIsClicked(true);
    setSearchParams({ click: 'learn-more' });
  }

  useEffect(
    function () {
      if (isClicked || searchParams.get('click') === 'learn-more') {
        ref?.current?.scrollIntoView({ behavior: 'smooth' });
      }

      setIsClicked(false);
    },
    [ref, isClicked, searchParams],
  );

  useEffect(
    function () {
      if (isPricingClicked || searchParams.get('click') === 'pricing') {
        pricingRef?.current?.scrollIntoView({ behavior: 'smooth' });
      }

      setIsPricingClicked(false);
    },
    [
      isPricingClicked,
      pricingRef,
      setIsPricingClicked,
      setSearchParams,
      searchParams,
    ],
  );

  useEffect(
    function () {
      setContentRef(testRef);
    },
    [testRef],
  );

  useEffect(
    function () {
      setIsMenuOpen(false);
    },
    [setIsMenuOpen],
  );

  return (
    <div className={`flex flex-auto flex-col`}>
      <StickyHeaders
        contentRef={contentRef}
        handleClickScrollToTop={handleClickScrollToTop}
        handleClickScrollToPricing={handleClickScrollToPricing}
      />

      <LandingPage
        handleClickScrollToLearnMore={handleClickScrollToLearnMore}
        handleClickScrollToPricing={handleClickScrollToPricing}
        setTestRef={setTestRef}
      />

      <HomepageSecondSection setRef={setRef} />
      <HomepageThirdSection setPricingRef={setPricingRef} />
    </div>
  );
}

export default Homepage;
