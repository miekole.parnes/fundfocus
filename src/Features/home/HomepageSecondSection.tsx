import { Dispatch, MutableRefObject, useEffect, useRef } from 'react';

function HomepageSecondSection({
  setRef,
}: {
  setRef: Dispatch<MutableRefObject<HTMLDivElement | null>>;
}) {
  const ref = useRef<HTMLDivElement | null>(null);

  useEffect(
    function () {
      setRef(ref);
    },
    [setRef],
  );
  return (
    <div
      className='flex flex-col items-center gap-y-10 self-center bg-white px-2 py-20 xl:mx-40 2xl:mx-60 wide:mx-[30rem]'
      ref={ref}
    >
      <p className='pb-10 pt-5 text-center text-4xl font-bold lg:text-5xl'>
        Manged your wealth easily
      </p>
      <div className='space-y-10'>
        <div className='flex flex-col gap-x-5 lg:flex-row'>
          <div className='flex flex-grow flex-col gap-y-5 p-2'>
            <p className='text-2xl font-bold'>Lorem ipsum</p>
            <p className='text-sm sm:text-base'>
              dolor sit amet consectetur adipisicing elit. Rerum laboriosam,
              accusantium expedita incidunt sapiente fuga cumque.
            </p>
          </div>
          <img
            src='/src/assets/img/img1.png'
            alt=''
            className='object-contain md:h-96 lg:h-80'
          />
        </div>
        <div className='flex flex-col-reverse gap-x-5 lg:flex-row'>
          <img
            src='/src/assets/img/img2.png'
            alt=''
            className='object-contain md:h-96 lg:h-80'
          />
          <div className='flex flex-col gap-y-5 p-2'>
            <p className='text-2xl font-bold'>Lorem ipsum</p>
            <p className='text-sm sm:text-base'>
              dolor sit amet consectetur adipisicing elit. Rerum laboriosam,
              accusantium expedita incidunt sapiente fuga cumque.
            </p>
          </div>
        </div>
        <div className='flex flex-col gap-x-5 lg:flex-row'>
          <div className='flex flex-col gap-y-5 p-2'>
            <p className='text-2xl font-bold'>Lorem ipsum</p>
            <p className='text-sm sm:text-base'>
              dolor sit amet consectetur adipisicing elit. Rerum laboriosam,
              accusantium expedita incidunt sapiente fuga cumque.
            </p>
          </div>
          <img
            src='/src/assets/img/img3.png'
            alt=''
            className='object-contain md:h-96 lg:h-80'
          />
        </div>
      </div>
    </div>
  );
}

export default HomepageSecondSection;
