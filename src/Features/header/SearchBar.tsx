function SearchBar() {
  return (
    <input
      type='text'
      className='border px-2 py-0.5 text-sm outline-none transition-all duration-300 lg:w-60 lg:focus:w-96'
      placeholder='Search'
    />
  );
}

export default SearchBar;
