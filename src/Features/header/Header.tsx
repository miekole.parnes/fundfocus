import { Link } from 'react-router-dom';
import { useData } from '../context/DataContext';
import { IoMenu } from 'react-icons/io5';
import Menu from './Menu';

function Header() {
  const {
    isMenuOpenContextValue: { setIsMenuOpen },
  } = useData();

  return (
    <header className='relative flex items-center justify-center bg-main sm:justify-between'>
      <div className='relative flex flex-grow items-center justify-center text-3xl font-bold lg:justify-start lg:text-4xl'>
        <button
          className='absolute left-2 lg:hidden'
          onClick={() => setIsMenuOpen(true)}
        >
          <IoMenu />
        </button>
        <Menu />
        <Link
          to={'/home'}
          className='flex items-center py-2 text-white lg:px-5 lg:py-5'
        >
          <p className='text-text2'>F</p>
          <p className='drop-shadow'>und</p> <p className='text-text2'>F</p>
          <p className='drop-shadow'>ocus</p>
        </Link>
      </div>
      <div className='hidden gap-x-10 px-5 text-lg font-semibold text-text2 lg:flex lg:gap-x-20'>
        <p>Home</p>
        <Link to='/home?click=pricing'>Pricing</Link>
        <p>About us</p>
      </div>
    </header>
  );
}

export default Header;
