import { MutableRefObject, useEffect, useState } from 'react';

import { IoMenu } from 'react-icons/io5';
import Menu from './Menu';
import { useData } from '../context/DataContext';

export default function StickyHeaders({
  contentRef,
  handleClickScrollToTop,
  handleClickScrollToPricing,
}: {
  contentRef?: MutableRefObject<HTMLDivElement | null>;
  handleClickScrollToPricing: () => void;

  handleClickScrollToTop: () => void;
}) {
  const [isSticky, setIsSticky] = useState(false);

  const {
    isMenuOpenContextValue: { setIsMenuOpen },
  } = useData();

  useEffect(() => {
    const handleIntersection = (entries: IntersectionObserverEntry[]) => {
      const [entry] = entries;

      setIsSticky(!entry.isIntersecting);
    };

    // Adjust rootMargin based on screen size

    const observer = new IntersectionObserver(handleIntersection, {
      root: null,
      rootMargin: '0px',
      threshold: 0.1,
    });

    const currentContentRef = contentRef?.current;

    if (currentContentRef) {
      observer.observe(currentContentRef);
    }

    return () => {
      if (currentContentRef) {
        observer.unobserve(currentContentRef);
      }
    };
  }, [contentRef]);

  return (
    <header
      className={`${
        isSticky
          ? 'fixed top-0 z-50 flex w-full items-center justify-center bg-main/95 shadow-md lg:justify-between'
          : 'hidden'
      }`}
    >
      <div className='relative flex flex-grow items-center justify-center text-3xl font-bold lg:justify-start lg:text-4xl'>
        <button
          className='absolute left-2 lg:hidden'
          onClick={() => setIsMenuOpen(true)}
        >
          <IoMenu />
        </button>
        <Menu />
        <button
          onClick={handleClickScrollToTop}
          className='flex items-center py-2 text-white lg:px-5 lg:py-5'
        >
          <p className='text-text2'>F</p>
          <p className='drop-shadow'>und</p> <p className='text-text2'>F</p>
          <p className='drop-shadow'>ocus</p>
        </button>
      </div>
      <div className='hidden text-xl font-semibold text-secondary lg:flex lg:gap-x-20 lg:pr-5'>
        <button onClick={handleClickScrollToTop}>Home</button>
        <button onClick={handleClickScrollToPricing}>Pricing</button>
        <p>About us</p>
      </div>
    </header>
  );
}
