import {
  createBrowserRouter,
  Navigate,
  RouterProvider,
} from 'react-router-dom';
import AppLayout from './Features/interface/AppLayout';
import Homepage from './Features/home/Homepage';
import Map from './Features/interface/Map';
import RegisterPage, {
  action as RegisterPageAction,
} from './Features/register/RegisterPage';
import { DataProvider } from './Features/context/DataContext';

const router = createBrowserRouter([
  {
    element: <AppLayout />,

    children: [
      {
        path: '/home',
        element: <Homepage />,
      },

      { path: '/map', element: <Map /> },
      {
        path: 'register',
        element: <RegisterPage />,
        action: RegisterPageAction,
      },
      { path: '/', element: <Navigate to='/home' /> },
    ],
  },
]);

function App() {
  return (
    <DataProvider>
      <RouterProvider router={router} />
    </DataProvider>
  );
}

export default App;
